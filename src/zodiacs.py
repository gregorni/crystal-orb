# zodiacs.py
#
# Copyright 2023 Fortune Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

WESTERN_ZODIAC_LIST = {
    "aries": {
        "name": _("Aries"),  # Translators: This is a zodiac sign (Western)
    },
    "taurus": {
        "name": _("Taurus"),  # Translators: This is a zodiac sign (Western)
    },
    "gemini": {
        "name": _("Gemini"),  # Translators: This is a zodiac sign (Western)
    },
    "cancer": {
        "name": _("Cancer"),  # Translators: This is a zodiac sign (Western)
    },
    "leo": {
        "name": _("Leo"),  # Translators: This is a zodiac sign (Western)
    },
    "virgo": {
        "name": _("Virgo"),  # Translators: This is a zodiac sign (Western)
    },
    "libra": {
        "name": _("Libra"),  # Translators: This is a zodiac sign (Western)
    },
    "scorpio": {
        "name": _("Scorpio"),  # Translators: This is a zodiac sign (Western)
    },
    "sagittarius": {
        "name": _("Sagittarius"),  # Translators: This is a zodiac sign (Western)
    },
    "capricorn": {
        "name": _("Capricorn"),  # Translators: This is a zodiac sign (Western)
    },
    "aquarius": {
        "name": _("Aquarius"),  # Translators: This is a zodiac sign (Western)
    },
    "pisces": {
        "name": _("Pisces"),  # Translators: This is a zodiac sign (Western)
    },
}

ASIAN_ZODIAC_LIST = {
    "monkey": {
        "name": _("Monkey"),  # Translators: This is a zodiac sign (Asian)
    },
    "rooster": {
        "name": _("Rooster"),  # Translators: This is a zodiac sign (Asian)
    },
    "dog": {
        "name": _("Dog"),  # Translators: This is a zodiac sign (Asian)
    },
    "pig": {
        "name": _("Pig"),  # Translators: This is a zodiac sign (Asian)
    },
    "rat": {
        "name": _("Rat"),  # Translators: This is a zodiac sign (Asian)
    },
    "ox": {
        "name": _("Ox"),  # Translators: This is a zodiac sign (Asian)
    },
    "tiger": {
        "name": _("Tiger"),  # Translators: This is a zodiac sign (Asian)
    },
    "rabbit": {
        "name": _("Rabbit"),  # Translators: This is a zodiac sign (Asian)
    },
    "dragon": {
        "name": _("Dragon"),  # Translators: This is a zodiac sign (Asian)
    },
    "snake": {
        "name": _("Snake"),  # Translators: This is a zodiac sign (Asian)
    },
    "horse": {
        "name": _("Horse"),  # Translators: This is a zodiac sign (Asian)
    },
    "goat": {
        "name": _("Goat"),  # Translators: This is a zodiac sign (Asian)
    },
}
