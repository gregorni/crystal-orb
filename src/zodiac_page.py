# zodiac_page.py
#
# Copyright 2023 Fortune Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import datetime

from gi.repository import Adw, Gtk


@Gtk.Template(resource_path="/io/gitlab/gregorni/Fortune/gtk/zodiac-page.ui")
class ZodiacPage(Adw.Bin):
    __gtype_name__ = "ZodiacPage"

    page = Gtk.Template.Child()

    details_clamp= Gtk.Template.Child()
    strengths_row = Gtk.Template.Child()
    weaknesses_row = Gtk.Template.Child()
    compatibility_row = Gtk.Template.Child()

    date_select_cal = Gtk.Template.Child()
    date_select_pop = Gtk.Template.Child()
    date_select_btn = Gtk.Template.Child()

    def __init__(self, parent, date_get_function, zodiacs_list, **kwargs):
        super().__init__(**kwargs)

        self.date_select_cal.connect("day-selected", parent.evaluate_date)
        self.date_get_function = date_get_function
        self.zodiacs_list = zodiacs_list

    def set_details(self, date_selected):
        self.details_clamp.show()

        date_selected_short = datetime.fromisoformat(
            date_selected.format_iso8601()[:-3]
        )
        cal_btn_label = str(date_selected_short)[:-9]
        sign = self.date_get_function(date_selected_short).lower()

        self.page.set_description(None)
        self.page.set_title(self.zodiacs_list[sign].get("name"))
        self.page.set_icon_name(sign)

        self.date_select_cal.select_day(date_selected)
        self.date_select_btn.set_label(cal_btn_label)
