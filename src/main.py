# main.py
#
# Copyright 2023 Fortune Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys

import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")

from gi.repository import Adw, Gio, Gtk

from .window import FortuneWindow


class FortuneApplication(Adw.Application):
    """The main application singleton class."""

    def __init__(self):
        super().__init__(
            application_id="io.gitlab.gregorni.Fortune",
            flags=Gio.ApplicationFlags.DEFAULT_FLAGS,
        )
        self.create_action("quit", lambda *_: self.quit(), ["<primary>q"])
        self.create_action("about", self.on_about_action)

    def do_activate(self):
        """Called when the application is activated.

        We raise the application's main window, creating it if
        necessary.
        """
        win = self.props.active_window
        if not win:
            win = FortuneWindow(application=self)
        win.present()

    def on_about_action(self, *args):
        """If you contributed code to the project,
        feel free to add yourself to the devs list.
        To add yourself into the list, you can add your
        name/username, and optionally an email or URL:

        Name only:    gregorni
        Name + URL:   gregorni https://gitlab.com/gregorni/
        Name + Email: gregorni <gregorniehl@web.de>
        """
        # This is a Python list: Add your string to the list (separated by a comma)
        devs_list = ["gregorni https://gitlab.com/gregorni"]

        """Callback for the app.about action."""
        about = Adw.AboutWindow(
            transient_for=self.props.active_window,
            application_name=_("Fortune"),
            application_icon="io.gitlab.gregorni.Fortune",
            developer_name="gregorni",
            version="1.0",
            developers=devs_list,
            # artists=[""],
            # Translators: Translate this string as your translator credits.
            # Name only:    gregorni
            # Name + URL:   gregorni https://gitlab.com/gregorni/
            # Name + Email: gregorni <gregorniehl@web.de>
            # Do not remove existing names.
            # Names are separated with newlines.
            translator_credits=_("translators-credits"),
            copyright=_("Copyright © 2023 Fortune Contributors"),
            license_type=Gtk.License.GPL_3_0,
            website="https://gitlab.com/gregorni/Fortune",
            issue_url="https://gitlab.com/gregorni/Fortune/-/issues",
            support_url="https://matrix.to/#/#gregorni-apps:matrix.org",
        )

        about.add_legal_section(
            title="python-zodiac-sign",
            copyright="Copyright © 2019 Kuznetsov Aleksey",
            license_type=Gtk.License.MIT_X11,
        )

        about.add_legal_section(
            title="Chinese-New-Year-and-Zodiac",
            copyright="Copyright © 2023 Nicolas Flandrois",
            license_type=Gtk.License.MIT_X11,
        )

        about.present()

    def create_action(self, name, callback, shortcuts=None):
        """Add an application action.

        Args:
            name: the name of the action
            callback: the function to be called when the action is
              activated
            shortcuts: an optional list of accelerators
        """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    """The application's entry point."""
    app = FortuneApplication()
    return app.run(sys.argv)
