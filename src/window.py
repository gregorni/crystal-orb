# window.py
#
# Copyright 2023 Fortune Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import zodiac_sign
from CnyZodiac import ChineseNewYearZodiac as cnyz
from gi.repository import Adw, Gio, Gtk

from . import zodiacs
from .zodiac_page import ZodiacPage


@Gtk.Template(resource_path="/io/gitlab/gregorni/Fortune/gtk/window.ui")
class FortuneWindow(Adw.ApplicationWindow):
    __gtype_name__ = "FortuneWindow"

    western_bin = Gtk.Template.Child()
    asian_bin = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        settings = Gio.Settings(schema_id="io.gitlab.gregorni.Fortune")
        settings.bind("width", self, "default-width", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("height", self, "default-height", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("is-maximized", self, "maximized", Gio.SettingsBindFlags.DEFAULT)

        self.western_page = ZodiacPage(
            self, zodiac_sign.get_zodiac_sign, zodiacs.WESTERN_ZODIAC_LIST
        )
        self.asian_page = ZodiacPage(
            self, cnyz().zodiac_date, zodiacs.ASIAN_ZODIAC_LIST
        )

        self.western_bin.set_child(self.western_page)
        self.asian_bin.set_child(self.asian_page)

    def evaluate_date(self, calendar, *args):
        date_selected = calendar.get_date()

        self.western_page.set_details(date_selected)
        self.asian_page.set_details(date_selected)
